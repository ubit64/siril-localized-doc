Siril - localized documentation
===============================

This repository contains localized documentation for Siril. It
is based on master document placed in siril-doc repository.

To translate edit your language file in po directory, changes will be
propagated to generated documents.

Requirements
------------

For manipulating with translations, you need gettext and sphinx installed.
If you want to build the documentation before submitting, you will also need
additional python modules, as listed in requirements.txt. To install them at once:

.. code-block:: sh

    python3 -m pip install -r requirements.txt

Usage (contributors)
--------------------

* Fork this repository

.. code-block:: sh

    git clone https://gitlab.com/free-astro/siril-localized-doc.git
    cd siril-localized-doc
    git submodule update --init

* Translate the po file for the language of your choice, say for French translation:
  `po/fr.po`

| This can be done with a simple txt editor or with dedicated tools like `Poedit. <https://poedit.net/>`_

* Submit a MR with your changes

..  warning::

  The syntax must follow strictly ReStructured Text formatting. You can find useful ressources in 
  `siril-doc useful links section <https://gitlab.com/free-astro/siril-doc#useful-links>`_



Usage (maintainers)
-------------------

To start new translation, open a shell in `tools/` and run:

.. code-block:: sh

    python3 tools/create_new.py
    python3 tools/update_folders.py
    python3 tools/update_po.py
    python3 tools/update_mo.py

This will:

* Create a new language subfolder in `docs/`
* Make symlinks to the files from `siril-doc/docs`
* Create a valid `conf.py` file for that language
* Add the language to the `LANGUAGES` section of the Makefile.
* In the `./po/` folder, copy `siril-documentation.pot` to create a new `.po` file.
* Create a new folder in `./translated/` to a new folder and make the necessary symlinks.

To update the siril-doc submodule:

* Start by pulling the submodule and commit the changes (`git submodule update --remote`)
* Same as above, just ommiting the `create_new` step
* commit the changes

To build documentation in given language, say french:

.. code-block:: sh

    cd docs
    sphinx-build fr fr/_build

You can also browse French translated documentation online at `readthedocs. <https://siril.readthedocs.io/fr/latest/>`_

Statistics
----------

.. code-block:: sh

    python3 tools/locale_stats.py

should output something like

.. code-block:: text

    Statistics from de.po
    2813 translated messages, 9 fuzzy translations, 49 untranslated messages.

    Statistics from fr.po
    78 translated messages, 2793 untranslated messages.