import os
from pathdefs import docs, base
from pathlib import Path
from subprocess import Popen, PIPE, STDOUT

def main():
    #update templates in locale/
    print('updating siril-documentation.pot')
    cmd = 'sphinx-build -b gettext -D gettext_compact=siril-documentation siril-doc/doc po'
    p = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=STDOUT, shell=True)
    p.communicate() #enables to wait till completion

    os.chdir(docs)
    dirs = Path('.').glob('*')
    dirs_list = [str(d) for d in dirs]
    os.chdir(os.path.join(base, 'po'))

    # updating all ./docs/* subfolders
    for d in dirs_list:
        pofile = '{:}.po'.format(d)
        print('Updating {:s}'.format(pofile))
        if not Path(pofile).is_file():
            cmd3 = 'msginit --no-translator -i {:s} -o {:s} -l {:s}'.format('siril-documentation.pot', pofile, d)
            p3 = Popen(cmd3, stdout=PIPE, stdin=PIPE, stderr=STDOUT, shell=True)
            p3.communicate()
        cmd4 = 'msgmerge -U {:s} {:s}'.format(pofile,'siril-documentation.pot')
        p4 = Popen(cmd4, stdout=PIPE, stdin=PIPE, stderr=STDOUT, shell=True)
        p4.communicate()



if __name__ == "__main__":
    main()
